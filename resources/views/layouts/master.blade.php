<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.meta')
    @include('layouts.css')
    

</head>

<body class="fix-header fix-sidebar card-no-border logo-center">

    @include('layouts.preload')

    <div id="main-wrapper">

        @include('layouts.header')


        @include('layouts.nav')

        <div class="page-wrapper">

            <div class="container-fluid">
                @include('layouts.bread_crumb')
                @yield('content')

            </div>
        </div>



        @include('layouts.customize_layout')

        @include('layouts.footer')

    </div>
    @include('layouts.script')
</body>

</html>