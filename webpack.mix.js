let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//Admin

mix.scripts([
'resources/assets/plugins/jquery/jquery.min.js',
'resources/assets/plugins/popper/popper.min.js',
'resources/assets/plugins/bootstrap/js/bootstrap.min.js',
'resources/assets/js/jquery.slimscroll.js',
'resources/assets/js/waves.js',
'resources/assets/js/sidebarmenu.js',
'resources/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
'resources/assets/plugins/sparkline/jquery.sparkline.min.js',
'resources/assets/js/custom.min.js',
'resources/assets/plugins/chartist-js/dist/chartist.min.js',
'resources/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js',
'resources/assets/plugins/d3/d3.min.js',
'resources/assets/plugins/c3-master/c3.min.js',
'resources/assets/js/dashboard1.js',
'resources/assets/plugins/styleswitcher/jQuery.style.switcher.js',
], 'public/js/master.js')
.styles([
    'resources/assets/plugins/bootstrap/css/bootstrap.min.css',
    'resources/assets/plugins/chartist-js/dist/charcotist.min.css',
    'resources/assets/plugins/chartist-js/dist/chartist-init.css',
    'resources/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
    'resources/assets/plugins/c3-master/c3.min.css',
    'resources/assets/css/style.css',
    'resources/assets/css/themify-icons.css',
    'resources/assets/css/font-awesome.min.css',
    'resources/assets/css/materialdesignicons.min.css',
    'resources/assets/css/blue.css',
], 'public/css/master.css');